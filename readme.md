#PRE-REQUIS 

* Java 8 or Java 11
* Maven

## 1 Installation Maven

```bash
sudo apt install maven
mvn --version
```

## 2 Installation Java

```bash
sudo apt install default-jdk
java --version
```

## COMMENT FAIRE FONCTIONNER LE TOUT AVEC DOCKER

```bash
$ git clone https://gitlab.com/GregLebreton/java-docker
$ mvn clean package
$ sudo docker build -t spring-boot:1.0 .
```
### run it
```bash
$ sudo docker run -d -p 8080:8080 -t spring-boot:1.0
```

### http://localhost:8080

### Doc: https://hub.docker.com/_/adoptopenjdk

